# Zig Kafka

## Introduction

A client library for Zig based on [librdkafka](https://github.com/edenhill/librdkafka).

## Getting started

Dependencies:
- crypto
- curl
- ssl
- z
- zstd

Build zig-kafka:
```sh
zig build
```

Produce a message:
```sh
./zig-out/bin/producer kafka.local:9094 zig-topic
```

Consume messages:
```sh
./zig-out/bin/consumer kafka.local:9094 zig-topic
```

It's also possible to run the producer/consumer examples from zig build:
```sh
zig build run-producer -- kafka.local:30094 zig-topic
zig build run-consumer -- kafka.local:30094 zig-topic
```

By specifying the parameter `-f` it is possible to run a consumer with a JSON configuration:
```sh
./zig-out/bin/consumer -f $PWD/examples/config/consumer.json
```

For the producer `-f` runs with an example with a Kafka properties config:
```sh
./zig-out/bin/producer -f $PWD/examples/config/producer.properties zig-topic
```

## Installation

Add dependency to `build.zig.zon`:
```sh
zig fetch --save https://codeberg.org/appler/zig-kafka/archive/v0.0.1-alpha.tar.gz
```

Update `build.zig` with import for module:
```zig
pub fn build(b: *std.Build) void {
    // Add module from dependency
    const kafka_mod = b.dependency("zig-kafka", .{
        .target = target,
        .optimize = optimize
    }).module("kafka");

    // Add module import
    your_compilation.root_module.addImport("kafka", kafka_mod);
}
```

Import `zig-kafka` to your project:
```zig
const kafka = @import("kafka");
```
