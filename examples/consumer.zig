const std = @import("std");
const fs = std.fs;
const heap = std.heap;
const io = std.io;
const json = std.json;
const math = std.math;
const mem = std.mem;
const process = std.process;

const argsWithAllocator = process.argsWithAllocator;
const cwd = fs.cwd;
const eql = mem.eql;
const maxInt = math.maxInt;
const parseFromSlice = json.parseFromSlice;
const getStdOut = io.getStdOut;
const bufferedWriter = io.bufferedWriter;
const getStdErr = io.getStdErr;

const Allocator = mem.Allocator;
const ArrayList = std.ArrayList;
const GeneralPurposeAllocator = heap.GeneralPurposeAllocator;

const kafka = @import("kafka");
const Consumer = kafka.Consumer;
const KafkaConfig = kafka.KafkaConfig;

const ConsumerConfig = struct {
    bootstrap_servers: []const u8,
    security_protocol: []const u8,
    group_id: []const u8,
    auto_offset_reset: []const u8,
    topic: []const u8,
};

pub fn main() !void {
    var gpa = GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();

    var args = ArrayList([]const u8).init(allocator);
    defer args.deinit();
    var args_iter = try argsWithAllocator(allocator);
    defer args_iter.deinit();
    while (args_iter.next()) |arg| {
        try args.append(arg);
    }

    var bootstrap_servers: []const u8 = "localhost:9092";
    var security_protocol: []const u8 = "PLAINTEXT";
    var group_id: []const u8 = "consumer-group";
    var auto_offset_reset: []const u8 = "earliest";
    var topic: []const u8 = "zig-topic";

    var conf = try KafkaConfig.init(allocator);
    defer conf.deinit();

    if (eql(u8, args.items[1], "-f")) {
        const fname = args.items[2];
        const json_content = try readFile(allocator, fname);

        const parsed = try parseFromSlice(ConsumerConfig, allocator, json_content, .{});
        defer parsed.deinit();

        bootstrap_servers = parsed.value.bootstrap_servers;
        security_protocol = parsed.value.security_protocol;
        group_id = parsed.value.group_id;
        auto_offset_reset = parsed.value.auto_offset_reset;
        topic = parsed.value.topic;
    } else {
        bootstrap_servers = args.items[1];
        topic = args.items[2];
    }

    try conf.set("bootstrap.servers", bootstrap_servers);
    try conf.set("security.protocol", security_protocol);
    try conf.set("group.id", group_id);
    try conf.set("auto.offset.reset", auto_offset_reset);
    try conf.dump();

    try writeStdOut("Start subscribing....\n", .{});
    var consumer = try Consumer.init(allocator, conf);
    defer consumer.deinit();
    try consumer.subscribe(topic);

    while (true) {
        var message = try consumer.consume();
        defer message.deinit();
        try writeStdOut("{?s}\n", .{message.payload});
    }
}

/// Opens file and returns contents. Remember to free contents.
fn readFile(allocator: Allocator, fname: []const u8) ![]u8 {
    var file_handle = try cwd().openFile(fname, .{});
    defer file_handle.close();

    const contents = try file_handle.readToEndAlloc(allocator, maxInt(usize));

    return contents;
}

fn writeStdOut(comptime format: []const u8, args: anytype) !void {
    const stdout_file = getStdOut().writer();
    try writer(stdout_file, format, args);
}

fn writer(underlying_stream: anytype, comptime format: []const u8, args: anytype) !void {
    var bw = bufferedWriter(underlying_stream);
    const stdout = bw.writer();
    try stdout.print(format, args);
    try bw.flush();
}
