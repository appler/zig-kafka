const std = @import("std");
const heap = std.heap;
const io = std.io;
const mem = std.mem;
const process = std.process;

const argsWithAllocator = process.argsWithAllocator;
const bufferedWriter = io.bufferedWriter;
const eql = mem.eql;
const getStdErr = io.getStdErr;
const getStdIn = io.getStdIn;
const getStdOut = io.getStdOut;

const ArrayList = std.ArrayList;
const GeneralPurposeAllocator = heap.GeneralPurposeAllocator;

const kafka = @import("kafka");
const KafkaConfig = kafka.KafkaConfig;
const MessageRequest = kafka.MessageRequest;
const MessageResponse = kafka.MessageResponse;
const Producer = kafka.Producer;

pub fn main() !void {
    var gpa = GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();

    var args = ArrayList([]const u8).init(allocator);
    defer args.deinit();
    var args_iter = try argsWithAllocator(allocator);
    defer args_iter.deinit();
    while (args_iter.next()) |arg| {
        try args.append(arg);
    }

    var conf = try KafkaConfig.init(allocator);
    defer conf.deinit();
    var topic: []const u8 = undefined;
    if (eql(u8, args.items[1], "-f")) {
        const path = args.items[2];
        try conf.load(path);
        topic = args.items[3];
    } else {
        const bootstrap_servers = args.items[1];
        try conf.set("bootstrap.servers", bootstrap_servers);
        try conf.set("security.protocol", "PLAINTEXT");
        topic = args.items[2];
    }

    try conf.dump();
    writeStdOut("All your {s} are belong to us.\n", .{"kafka producing"});

    var producer = try Producer.init(allocator, conf, callback);
    defer producer.deinit();
    while (true) {
        writeStdOut("Enter some text: ", .{});
        const stdin = getStdIn().reader();
        const message = (try stdin.readUntilDelimiterOrEofAlloc(allocator, '\n', 4096)).?;
        defer allocator.free(message);

        try producer.send(MessageRequest{
            .topic = topic,
            .key = "12345",
            .payload = message,
        });
        writeStdOut("\n", .{});
    }
}

pub fn callback(res: MessageResponse) void {
    // this callback function cannot handle errors
    writeStdOut("Message delivered: {?s}\n", .{res.payload});
}

var stdout_mutex = std.Thread.Mutex{};

/// Write message to stdout and ignore any errors
fn writeStdOut(comptime format: []const u8, args: anytype) void {
    stdout_mutex.lock();
    defer stdout_mutex.unlock();
    const stdout_file = getStdOut().writer();
    nosuspend writer(stdout_file, format, args) catch return;
}

fn writer(underlying_stream: anytype, comptime format: []const u8, args: anytype) !void {
    var bw = bufferedWriter(underlying_stream);
    const stdout = bw.writer();
    try stdout.print(format, args);
    try bw.flush();
}
