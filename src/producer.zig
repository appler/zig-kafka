const std = @import("std");
const testing = std.testing;

const Allocator = std.mem.Allocator;

const KafkaConfig = @import("config.zig").KafkaConfig;
const MessageResponse = @import("message.zig").MessageResponse;
const MessageRequest = @import("message.zig").MessageRequest;
const k = @import("rdkafka.zig");
const log = @import("log.zig").get(.producer);

const ProduceMessageResult = enum(i8) {
    Success = 0,
    Error = -1,
};

const ProducerError = error {
    CreationFailed,
    SendMessageFailed,
} || Allocator.Error;

pub const Producer = struct {
    allocator: Allocator,
    native: ?*k.rd_kafka_t,

    pub fn init(allocator: Allocator, conf: KafkaConfig, comptime callbackFn: fn (res: MessageResponse) void) ProducerError!Producer {
        const Wrapper = struct {
            pub fn callback(rk: ?*k.rd_kafka_t, rkmessage: [*c]const k.rd_kafka_message_t, _: ?*anyopaque) callconv(.C) void {
                _ = rk;

                callbackFn(MessageResponse.map(rkmessage.*));
            }
        };
        const errstr_size = 512;
        var errstr: [*:0]u8 = undefined;
        errdefer log.err("{s}", .{ @as([*:0]u8, @ptrCast(&errstr)) });
        k.rd_kafka_conf_set_dr_msg_cb(conf.native, Wrapper.callback);

        const producer = k.rd_kafka_new(k.RD_KAFKA_PRODUCER, conf.native, @ptrCast(&errstr), errstr_size);
        if (producer == undefined) {
            return ProducerError.CreationFailed;
        }

        return Producer {
            .allocator = allocator,
            .native = producer
        };
    }

    pub fn deinit(self: *Producer) void {
        k.rd_kafka_destroy(self.native);
    }

    pub fn send(self: *Producer, message: MessageRequest) ProducerError!void {
        const topic_conf = k.rd_kafka_topic_conf_new();
        const partition = 0;
        const empty_key: [*:0]u8 = undefined;
        const tombstone: [*:0]u8 = undefined;
        const msg_opaque: [*:0]u8 = undefined;

        const rkt = k.rd_kafka_topic_new(self.native, @constCast(message.topic.ptr), topic_conf);
        const result = k.rd_kafka_produce(
            rkt,
            partition,
            k.RD_KAFKA_MSG_F_COPY,
            if (message.payload != null) @constCast(message.payload.?.ptr) else @as(?*anyopaque, tombstone),
            if (message.payload != null) message.payload.?.len else @as(c_int, 0),
            if (message.key != null) @constCast(message.key.?.ptr) else @as(?*anyopaque, empty_key),
            if (message.key != null) message.key.?.len else @as(c_int, 0),
            msg_opaque);

        if (result != 0) {
            log.err("send message failed with code: {d}", .{result});
            return ProducerError.SendMessageFailed;
        }

        const flush_res = k.rd_kafka_flush(self.native, 10 * 1000);

        if(flush_res != 0) {
            log.err("flush failed with code: {d}", .{flush_res});
        }
    }
};

test "init and deinit" {
    const allocator = testing.allocator;
    var conf = try KafkaConfig.init(allocator);
    defer conf.deinit();
    try conf.set("bootstrap.servers", "kafka.local:9094");

    const Wrapper = struct {
        pub fn callback(res: MessageResponse) void {
            _ = res;
        }
    };

    var producer = try Producer.init(allocator, conf, Wrapper.callback);
    defer producer.deinit();
}
