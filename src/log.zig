const std = @import("std");
const builtin = @import("builtin");
const io = std.io;

const getStdOut = io.getStdOut;
const bufferedWriter = io.bufferedWriter;
const getStdErr = io.getStdErr;

/// Gets a logger that will degrade error messages to debug for tests
pub fn get(comptime scope: @Type(.EnumLiteral)) type {
    return if (builtin.is_test)
        // Downgrade `err` to `debug` for tests.
        // Zig fails any test that does `log.err`, but we want to test those code paths.
        struct {
            pub const base = std.log.scoped(scope);
            pub const err = debug;
            pub const warn = base.warn;
            pub const info = base.info;
            pub const debug = base.debug;
        }
    else
        std.log.scoped(scope);
}

/// Write message to stdout
pub fn writeStdOut(comptime format: []const u8, args: anytype) !void {
    const stdout_file = getStdOut().writer();
    try writer(stdout_file, format, args);
}

var stderr_mutex = std.Thread.Mutex{};

/// Write message to stderr and ignore any errors
pub fn writeStdErr(comptime format: []const u8, args: anytype) void {
    stderr_mutex.lock();
    defer stderr_mutex.unlock();
    const stderr_file = getStdErr().writer();
    nosuspend writer(stderr_file, format, args) catch return;
}

fn writer(underlying_stream: anytype, comptime format: []const u8, args: anytype) !void {
    var bw = bufferedWriter(underlying_stream);
    const stdout = bw.writer();
    try stdout.print(format, args);
    try bw.flush();
}
