const std = @import("std");
const mem = std.mem;
const testing = std.testing;

const Allocator = mem.Allocator;
const k = @import("rdkafka.zig");
const log = @import("log.zig").get(.metadata);

const Producer = @import("./producer.zig").Producer;
const Consumer = @import("./consumer.zig").Consumer;
const Metadata = @This();

allocator: Allocator,
native: [*c]const k.struct_rd_kafka_metadata,
broker_id: i32,
broker_name: []u8,
brokers: []MetadataBroker,
topics: []MetadataTopic,

pub const Error = error {
    MetadataFetchError,
} || Allocator.Error || Producer.Error;

pub const MetadataBroker = struct {
    id: i32,
    host: []u8,
    port: i32,

    pub fn init(broker: *allowzero k.rd_kafka_metadata_broker) MetadataBroker {
        return MetadataBroker {
            .id = broker.id,
            .host = mem.span(broker.host),
            .port = @intCast(broker.port)
        };
    }
};

pub const MetadataTopic = struct {
    allocator: Allocator,
    name: []u8,
    partitions: []MetadataPartition,
    error_code: i32,

    pub fn init(topic: *allowzero k.struct_rd_kafka_metadata_topic, allocator: Allocator) Error!MetadataTopic {
        const partition_count: usize = @intCast(topic.partition_cnt);
        var partitions: []MetadataPartition = try allocator.alloc(MetadataPartition, partition_count);
        for (0..partition_count) |i| {
            partitions[i] = MetadataPartition.init(&topic.*.partitions[i]);
        }

        return MetadataTopic {
            .allocator = allocator,
            .name = mem.span(topic.topic),
            .partitions =  partitions,
            .error_code = @intCast(topic.err)
        };
    }

    pub fn deinit(self: MetadataTopic) void {
        self.allocator.free(self.partitions);
    }
};

pub const MetadataPartition = struct {
    id: i32,
    leader: i32,
    insync_replicas: []i32,
    replicas: []i32,
    error_code: i32,

    pub fn init(partition: *allowzero k.struct_rd_kafka_metadata_partition) MetadataPartition {
        return MetadataPartition {
            .id = @intCast(partition.id),
            .leader = @intCast(partition.leader),
            .insync_replicas = partition.isrs[0..@intCast(partition.isr_cnt)],
            .replicas = partition.replicas[0..@intCast(partition.replica_cnt)],
            .error_code = @intCast(partition.err)
        };
    }
};

pub fn fromProducer(producer: *Producer, allocator: Allocator) Error!Metadata {
    return try fromNative(producer.native, allocator);
}

pub fn fromConsumer(consumer: *Consumer, allocator: Allocator) Error!Metadata {
    return try fromNative(consumer.native, allocator);
}

pub fn fromNative(native: ?*k.rd_kafka_t, allocator: Allocator) Error!Metadata {
    var metadata: [*c]const k.struct_rd_kafka_metadata = undefined;

    const err = k.rd_kafka_metadata(native, @as(c_int, 1), null, &metadata, @as(c_int, 5000));
    errdefer log.err("Failed to fetch metadata. Code:{d}", .{ err });
    if (err != 0) {
        return Error.MetadataFetchError;
    }

    const topic_count: usize = @intCast(metadata.*.topic_cnt);
    var topics: []MetadataTopic = try allocator.alloc(MetadataTopic, topic_count);
    for (0..topic_count) |i| {
        topics[i] = try MetadataTopic.init(&metadata.*.topics[i], allocator);
    }

    const broker_count: usize = @intCast(metadata.*.broker_cnt);
    var brokers: []MetadataBroker = try allocator.alloc(MetadataBroker, broker_count);
    for (0..broker_count) |i| {
        brokers[i] = MetadataBroker.init(&metadata.*.brokers[i]);
    }

    return Metadata {
        .allocator = allocator,
        .native = metadata,
        .broker_id = @intCast(metadata.*.orig_broker_id),
        .broker_name = mem.span(metadata.*.orig_broker_name),
        .brokers = brokers,
        .topics = topics,
    };
}

pub fn deinit(self: Metadata) void {
    for (self.topics) |p| p.deinit();
    self.allocator.free(self.brokers);
    self.allocator.free(self.topics);
    k.rd_kafka_metadata_destroy(self.native);
}
