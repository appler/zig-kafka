const std = @import("std");
const testing = std.testing;

const k = @import("rdkafka.zig");

const Log = @import("log.zig");
const writeStdErr = Log.writeStdErr;

pub const MessageRequest = struct {
    key: ?[]const u8,
    payload: ?[]const u8,
    topic: []const u8,
};

pub const MessageResponse = struct {
    native: k.rd_kafka_message_t,
    err: usize,
    key: ?[]const u8,
    offset: i64,
    partition: i32,
    payload: ?[]const u8,

    pub fn map(response: k.rd_kafka_message_t) MessageResponse {
        const payload = if (response.payload != null) @as([*]u8, @ptrCast(response.payload.?))[0..response.len] else null;
        const key = if (response.key != null) @as([*]u8, @ptrCast(response.key.?))[0..response.key_len] else null;
        const err: usize = @intCast(response.err);
        if (err != 0) {
            writeStdErr("MessageRepsonse: error: code: {d}, message: {?s}.\n", .{ err, payload });
        }
        return MessageResponse{
            .native = response,
            .err = err,
            .key = key,
            .offset = response.offset,
            .partition = response.partition,
            .payload = payload,
        };
    }

    pub fn deinit(self: *MessageResponse) void {
        defer k.rd_kafka_message_destroy(&self.native);
    }
};

test "message response map" {
    const key = "12345";
    const payload = "message";

    const kafka_res = k.rd_kafka_message_t {
        .key = @as(*anyopaque, @constCast(key)),
        .key_len = key.len,
        .payload = @as(*anyopaque, @constCast(payload)),
        .len = payload.len,
        .err = 0,
    };

    const message_res = MessageResponse.map(kafka_res);

    try testing.expectEqual(key, message_res.key);
    try testing.expectEqual(payload, message_res.payload);
}

test "message response map null" {
    const kafka_res = k.rd_kafka_message_t {
        .key = null,
        .key_len = 0,
        .payload = null,
        .len = 0,
        .err = 0,
    };

    const message_res = MessageResponse.map(kafka_res);

    try testing.expectEqual(null, message_res.key);
    try testing.expectEqual(null, message_res.payload);
}
