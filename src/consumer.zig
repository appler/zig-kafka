const std = @import("std");
const mem = std.mem;
const testing = std.testing;

const Allocator = mem.Allocator;

const KafkaConfig = @import("config.zig").KafkaConfig;
const MessageResponse = @import("message.zig").MessageResponse;
const k = @import("rdkafka.zig");
const log = @import("log.zig").get(.consumer);

const SubscribeResult = enum(i8) {
    Success = 0,
    Error = -1,
};

const ConsumerError = error{
    CreationFailed,
    SubscribeFailed,
} || Allocator.Error;

pub const Consumer = struct {
    allocator: Allocator,
    native: ?*k.rd_kafka_t,
    pub fn init(allocator: Allocator, conf: KafkaConfig) ConsumerError!Consumer {
        const errstr_size = 512;
        var errstr: [*:0]u8 = undefined;
        errdefer log.err("{s}", .{@as([*:0]u8, @ptrCast(&errstr))});

        const consumer = k.rd_kafka_new(k.RD_KAFKA_CONSUMER, conf.native, @ptrCast(&errstr), errstr_size);
        if (consumer == undefined) {
            return ConsumerError.CreationFailed;
        }

        return Consumer{
            .allocator = allocator,
            .native = consumer,
        };
    }

    pub fn deinit(self: *Consumer) void {
        _ = k.rd_kafka_consumer_close(self.native);
        k.rd_kafka_destroy(self.native);
    }

    pub fn subscribe(self: *Consumer, topic: []const u8) ConsumerError!void {
        const topic_cnt = 1;
        _ = k.rd_kafka_poll_set_consumer(self.native);

        const topicZ = try self.allocator.dupeZ(u8, topic);
        defer self.allocator.free(topicZ);

        const sub = k.rd_kafka_topic_partition_list_new(topic_cnt);
        _ = k.rd_kafka_topic_partition_list_add(sub, topicZ, k.RD_KAFKA_PARTITION_UA);

        const res = k.rd_kafka_subscribe(self.native, sub);
        const sub_res: SubscribeResult = @enumFromInt(res);
        if (sub_res != SubscribeResult.Success) {
            return ConsumerError.SubscribeFailed;
        }

        k.rd_kafka_topic_partition_list_destroy(sub);
    }

    pub fn consume(self: *Consumer) ConsumerError!MessageResponse {
        var rkm = k.rd_kafka_consumer_poll(self.native, 100);
        while (rkm == null) {
            rkm = k.rd_kafka_consumer_poll(self.native, 100);
        }

        return MessageResponse.map(rkm.*);
    }
};

test "init and deinit" {
    const allocator = testing.allocator;
    var conf = try KafkaConfig.init(allocator);
    defer conf.deinit();
    try conf.set("bootstrap.servers", "kafka.local:9094");

    var consumer = try Consumer.init(allocator, conf);
    defer consumer.deinit();
}
