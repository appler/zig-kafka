const std = @import("std");
const builtin = @import("builtin");
const ini = @import("ini");
const fs = std.fs;
const io = std.io;
const mem = std.mem;
const testing = std.testing;

const eql = mem.eql;
const expect = testing.expect;
const fixedBufferStream = io.fixedBufferStream;

const Allocator = mem.Allocator;
const Reader = fs.File.Reader;

const k = @import("rdkafka.zig");
const Log = @import("log.zig");
const log = Log.get(.config);
const writeStdOut = Log.writeStdOut;

pub const KafkaConfigResult = enum(i8) {
    Ok = k.RD_KAFKA_CONF_OK,
    Invalid = k.RD_KAFKA_CONF_INVALID,
    Unknown = k.RD_KAFKA_CONF_UNKNOWN,
};

pub const KafkaConfigError = error{
    ConfigUnknown,
    ConfigInvalid,
} || Allocator.Error;

pub const KeyValue = struct { key: []const u8, value: []const u8 };

pub const KafkaConfig = struct {
    allocator: Allocator,
    file_conf: []KeyValue,
    map: std.StringArrayHashMap([]const u8),
    native: ?*k.rd_kafka_conf_t,

    pub fn init(allocator: Allocator) !KafkaConfig {
        return KafkaConfig {
            .allocator = allocator,
            .map = std.StringArrayHashMap([]const u8).init(allocator),
            .native = k.rd_kafka_conf_new(),
            .file_conf = &[_]KeyValue{}
        };
    }

    /// Loads config from a Kafka properties file
    pub fn load(self: *KafkaConfig, path: []const u8) !void {
        const file = try std.fs.cwd().openFile(path, .{});
        defer file.close();
        var parser = ini.parse(self.allocator, file.reader());
        defer parser.deinit();

        var kva = std.ArrayList(KeyValue).init(self.allocator);
        defer kva.deinit();
        while (try parser.next()) |record| {
            switch (record) {
                .section => {},
                .property => |kv| {
                    try kva.append(.{
                        .key = try self.allocator.dupe(u8, kv.key),
                        .value = try self.allocator.dupe(u8, kv.value)
                    });
                },
                .enumeration => {},
            }
        }

        self.file_conf = try kva.toOwnedSlice();

        for (self.file_conf) |kv| {
            try self.set(kv.key , kv.value);
        }
    }

    pub fn deinit(self: *KafkaConfig) void {
        for (self.file_conf) |kv| {
            self.allocator.free(kv.key);
            self.allocator.free(kv.value);
        }
        self.allocator.free(self.file_conf);
        self.map.deinit();
    }

    pub fn set(self: *KafkaConfig, key: []const u8, value: []const u8) KafkaConfigError!void {
        const errstr_size = 512;
        var errstr: [*:0]const u8 = undefined;
        errdefer log.err("{s}", .{@as([*:0]u8, @ptrCast(&errstr))});

        const keyZ = try self.allocator.dupeZ(u8, key);
        defer self.allocator.free(keyZ);
        const valueZ = try self.allocator.dupeZ(u8, value);
        defer self.allocator.free(valueZ);

        const res = k.rd_kafka_conf_set(self.native, keyZ, valueZ, @ptrCast(&errstr), errstr_size);
        const conf_res: KafkaConfigResult = @enumFromInt(res);

        return switch (conf_res) {
            KafkaConfigResult.Ok => {
                try self.map.put(key, value);
            },
            KafkaConfigResult.Invalid => KafkaConfigError.ConfigInvalid,
            KafkaConfigResult.Unknown => KafkaConfigError.ConfigUnknown,
        };
    }

    pub fn dump(self: *KafkaConfig) !void {
        try writeStdOut("Kafka config:\n", .{});
        var iter = self.map.iterator();
        while (iter.next()) |conf| {
            try writeStdOut("{s}={s}\n", .{ conf.key_ptr.*, conf.value_ptr.* });
        }
        try writeStdOut("\n", .{});
    }
};

test "kafka config set" {
    const allocator = testing.allocator;
    var conf = try KafkaConfig.init(allocator);
    defer conf.deinit();

    try conf.set("bootstrap.servers", "kafka.local:9094");
    const res_unknown = conf.set("brokers", "kafka.local:9094");
    const res_invalid = conf.set("sasl.jaas.config", "org.apache.kafka.common.security.plain.PlainLoginModule required username=\"admin\" password=\"admin-secret\";");

    try expect(eql(u8, "kafka.local:9094", conf.map.get("bootstrap.servers").?));
    try expect(res_unknown ==  KafkaConfigError.ConfigUnknown);
    try expect(res_invalid == KafkaConfigError.ConfigInvalid);
}

test "kafka config load file" {
    const allocator = testing.allocator;
    var conf = try KafkaConfig.init(allocator);
    defer conf.deinit();

    try conf.load("examples/config/producer.properties");

    try expect(eql(u8, "bootstrap.servers", conf.file_conf[0].key));
    try expect(eql(u8, "kafka.local:30094", conf.file_conf[0].value));
    try expect(eql(u8, "security.protocol", conf.file_conf[1].key));
    try expect(eql(u8, "PLAINTEXT", conf.file_conf[1].value));
}

test "kafka config combined load file and set" {
     const allocator = testing.allocator;
    var conf = try KafkaConfig.init(allocator);
    defer conf.deinit();

    try conf.load("examples/config/producer.properties");
    try conf.set("bootstrap.servers", "localhost:9092");

    try expect(eql(u8, "localhost:9092", conf.map.get("bootstrap.servers").?));
    try expect(eql(u8, "PLAINTEXT", conf.map.get("security.protocol").?));
}
