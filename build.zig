const std = @import("std");
const builtin = @import("builtin");

const Build = std.Build;
const Compile = Build.Step.Compile;
const ExecutableOptions = Build.ExecutableOptions;

const min_zig_string = "0.12.0-dev.2930+3eacd1b2e";

pub fn build(b: *Build) void {
    comptime {
        const min_zig = try std.SemanticVersion.parse(min_zig_string);
        if (builtin.zig_version.order(min_zig) == .lt) {
            const message = "Your Zig version is too low to build this project. Minimimum version: " ++ min_zig_string;
            @compileError(message);
        }
    }

    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});
    const os = target.result.os;

    const kafka_mod = b.addModule("kafka", .{
        .root_source_file = b.path("src/kafka.zig"),
        .target = target,
        .optimize = optimize
    });

    const exe_producer = b.addExecutable(.{
        .name = "producer",
        .root_source_file = b.path("examples/producer.zig"),
        .target = target,
        .optimize = optimize
    });
    const exe_consumer = b.addExecutable(.{
        .name = "consumer",
        .root_source_file = b.path("examples/consumer.zig"),
        .target = target,
        .optimize = optimize,
    });
    const exe_unit_tests = b.addTest(.{
        .root_source_file = b.path("src/kafka.zig"),
        .target = target,
        .optimize = optimize,
    });

    exe_producer.root_module.addImport("kafka", kafka_mod);
    exe_consumer.root_module.addImport("kafka", kafka_mod);
    exe_unit_tests.root_module.addImport("kafka", kafka_mod);

    buildExe(b,exe_consumer, "run-consumer");
    buildExe(b, exe_producer, "run-producer");

    const run_exe_unit_tests = b.addRunArtifact(exe_unit_tests);
    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_exe_unit_tests.step);

    if (os.tag == .windows) {
        // .dll and .lib files comes from librdkafka.redist.2.3.0 NuGet-package
        b.installFile("lib/librdkafka.dll", "bin/librdkafka.dll");
        b.installFile("lib/libcrypto-3-x64.dll", "bin/libcrypto-3-x64.dll");
        b.installFile("lib/libcurl.dll", "bin/libcurl.dll");
        b.installFile("lib/libssl-3-x64.dll", "bin/libssl-3-x64.dll");
        b.installFile("lib/zlib1.dll", "bin/zlib1.dll");
        b.installFile("lib/zstd.dll", "bin/zstd.dll");

        kafka_mod.addLibraryPath(b.path("lib"));
        kafka_mod.linkSystemLibrary("rdkafka", .{ .needed = true });
    } else {
        const rdkafka_upstream = b.dependency("librdkafka", .{});
        const rdkafka_lib = b.addStaticLibrary(.{
            .name = "rdkafka",
            .target = target,
            .optimize = optimize
        });

        const config_h = b.addConfigHeader(.{
            .style = .{
                .cmake = .{
                    .dependency = .{
                        .dependency = rdkafka_upstream,
                        .sub_path = "packaging/cmake/config.h.in"
                    }
                }
            }
        },.{
            .SOLIB_EXT = ".so",
            .ARCH = target.result.cpu.arch.genericName(),
            .CPU = "generic",
            .WITHOUT_OPTIMIZATION = 0,
            .WITH_STRIP = 0,
            .ENABLE_ZLIB = "try",
            .ENABLE_ZSTD = "try",
            .ENABLE_SSL = "try",
            .ENABLE_GSSAPI = "try",
            .ENABLE_CURL = "try",
            .ENABLE_DEVEL = 0,
            .ENABLE_VALGRIND = 0,
            .ENABLE_REFCNT_DEBUG = 0,
            .ENABLE_LZ4_EXT = 1,
            .ENABLE_REGEX_EXT = 1,
            .ENABLE_C11THREADS = "try",
            .ENABLE_SYSLOG = 1,
            .WITH_STATIC_LINKING = 1,
            .MKL_APP_NAME = "librdkafka",
            .MKL_APP_DESC_ONELINE = "The Apache Kafka C/C++ library",
            .WITH_GCC = 1,
            .WITH_GXX = 1,
            .WITH_PKGCONFIG = 1,
            .WITH_INSTALL = 1,
            .HAS_GNU_AR = 1,
            .HAVE_PIC = 1,
            .WITH_GNULD = 1,
            .HAVE_ATOMICS_32 = 1,
            .HAVE_ATOMICS_32_ATOMIC = 1,
            .HAVE_ATOMICS_64 = 1,
            .HAVE_ATOMICS_64_ATOMIC = 1,
            .RDKAFKA_VERSION_STR = "2.3.0",
            .MKL_APP_VERSION = "2.3.0",
            .WITH_C11THREADS = 1,
            .WITH_LIBDL = 1,
            .WITH_PLUGINS = 1,
            .WITH_ZLIB = 1,
            .WITH_SSL = 1,
            .OPENSSL_SUPPRESS_DEPRECATED = "OPENSSL_SUPPRESS_DEPRECATED",
            .WITH_ZSTD = 1,
            .WITH_CURL = 1,
            .WITH_HDRHISTOGRAM = 1,
            .WITH_SYSLOG = 1,
            .WITH_SNAPPY = 1,
            .WITH_SOCKEM = 1,
            .WITH_SASL_SCRAM = 1,
            .WITH_SASL_OAUTHBEARER = 1,
            .WITH_OAUTHBEARER_OIDC = 1,
            .WITH_CRC32C_HW = 1,
            .HAVE_REGEX = 1,
            .HAVE_RAND_R = 1,
            .HAVE_STRNDUP = 1,
            .HAVE_STRLCPY = 1,
            .HAVE_STRERROR_R = 1,
            .HAVE_STRCASESTR = 1,
            .HAVE_PTHREAD_SETNAME_GNU = 1,
            .HAVE_PYTHON = 1,
            .HAVE_GETRUSAGE = 1,
            .BUILT_WITH = "STATIC_LINKING GCC GXX PKGCONFIG INSTALL GNULD LDS C11THREADS LIBDL PLUGINS ZLIB SSL ZSTD CURL HDRHISTOGRAM SYSLOG SNAPPY SOCKEM SASL_SCRAM SASL_OAUTHBEARER OAUTHBEARER_OIDC CRC32C_HW",
        });

        rdkafka_lib.addConfigHeader(config_h);
        rdkafka_lib.addCSourceFiles(.{
            .root = rdkafka_upstream.path(""),
            .files = &rdkafka_sources,
        });

        rdkafka_lib.linkLibC();
        rdkafka_lib.linkSystemLibrary("crypto");
        rdkafka_lib.linkSystemLibrary("curl");
        rdkafka_lib.linkSystemLibrary("ssl");
        rdkafka_lib.linkSystemLibrary("z");
        rdkafka_lib.linkSystemLibrary("zstd");

        kafka_mod.linkLibrary(rdkafka_lib);

        // Add include to rdkafka.h
        exe_unit_tests.addIncludePath(rdkafka_upstream.path("src"));
        kafka_mod.addIncludePath(rdkafka_upstream.path("src"));
    }

    const ini_mod = b.dependency("ini", .{
        .target = target,
        .optimize = optimize
    }).module("ini");

    kafka_mod.addImport("ini", ini_mod);
    exe_unit_tests.root_module.addImport("ini", ini_mod);
}

fn buildExe(b: *Build,  exe: *Compile, run_name: [] const u8) void {
    b.installArtifact(exe);

    const run_cmd = b.addRunArtifact(exe);
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step(run_name, "Run the app");
    run_step.dependOn(&run_cmd.step);
}

const rdkafka_sources = [_][]const u8 {
    "src/rdkafka.c",
    "src/cJSON.c",
    "src/crc32c.c",
    "src/lz4.c",
    "src/lz4frame.c",
    "src/lz4hc.c",
    "src/rdaddr.c",
    "src/rdavl.c",
    "src/rdbase64.c",
    "src/rdbuf.c",
    "src/rdcrc32.c",
    "src/rddl.c",
    "src/rdfnv1a.c",
    "src/rdgz.c",
    "src/rdhdrhistogram.c",
    "src/rdhttp.c",
    "src/rdkafka_admin.c",
    "src/rdkafka_assignment.c",
    "src/rdkafka_assignor.c",
    "src/rdkafka_aux.c",
    "src/rdkafka_background.c",
    "src/rdkafka_broker.c",
    "src/rdkafka_buf.c",
    "src/rdkafka_cert.c",
    "src/rdkafka_cgrp.c",
    "src/rdkafka_conf.c",
    "src/rdkafka_coord.c",
    "src/rdkafka_error.c",
    "src/rdkafka_event.c",
    "src/rdkafka_feature.c",
    "src/rdkafka_fetcher.c",
    "src/rdkafka_header.c",
    "src/rdkafka_idempotence.c",
    "src/rdkafka_interceptor.c",
    "src/rdkafka_lz4.c",
    "src/rdkafka_metadata.c",
    "src/rdkafka_metadata_cache.c",
    "src/rdkafka_mock.c",
    "src/rdkafka_mock_cgrp.c",
    "src/rdkafka_mock_handlers.c",
    "src/rdkafka_msg.c",
    "src/rdkafka_msgset_reader.c",
    "src/rdkafka_msgset_writer.c",
    "src/rdkafka_offset.c",
    "src/rdkafka_op.c",
    "src/rdkafka_partition.c",
    "src/rdkafka_pattern.c",
    "src/rdkafka_plugin.c",
    "src/rdkafka_queue.c",
    "src/rdkafka_range_assignor.c",
    "src/rdkafka_request.c",
    "src/rdkafka_roundrobin_assignor.c",
    "src/rdkafka_sasl.c",
    "src/rdkafka_sasl_oauthbearer.c",
    "src/rdkafka_sasl_oauthbearer_oidc.c",
    "src/rdkafka_sasl_plain.c",
    "src/rdkafka_sasl_scram.c",
    "src/rdkafka_ssl.c",
    "src/rdkafka_sticky_assignor.c",
    "src/rdkafka_subscription.c",
    "src/rdkafka_timer.c",
    "src/rdkafka_topic.c",
    "src/rdkafka_transport.c",
    "src/rdkafka_txnmgr.c",
    "src/rdkafka_zstd.c",
    "src/rdlist.c",
    "src/rdlog.c",
    "src/rdmap.c",
    "src/rdmurmur2.c",
    "src/rdports.c",
    "src/rdrand.c",
    "src/rdregex.c",
    "src/rdstring.c",
    "src/rdunittest.c",
    "src/rdvarint.c",
    "src/rdxxhash.c",
    "src/regexp.c",
    "src/snappy.c",
    "src/tinycthread.c",
    "src/tinycthread_extra.c"
};
